import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class App {
    public static void main(String[] args) throws Exception {
        // task 1
        String str = "Devcamp";
        int[] arr = { 1, 2, 3 };
        String[] parts = "Devcamp User".split(" ");

        System.out.println("Task 1: ");
        System.out.println("Is str array: " + task1(str)); // false
        System.out.println("Is arr array: " + task1(arr)); // true
        System.out.println("Is parts array: " + task1(parts)); // true

        // task 2
        int[] arr1 = { 1, 2, 3, 4, 5, 6 };
        System.out.println("Task 2: ");
        System.out.println("Phần tử của arr1 khi n = 3 là: " + task2(arr1, 3));
        System.out.println("Phần tử của arr1 khi n = 6 là: " + task2(arr1, 6));

        // task 3
        int[] arr2 = { 3, 8, 7, 6, 5, -4, -3, 2, 1 };
        System.out.println("Task 3: ");
        System.out.println("Mang arr2 sau khi sap xep tang dan la: ");
        task3(arr2);

        // // task 4
        int[] arr3 = { 1, 2, 3, 4, 5, 6 };
        task4(arr3, 3);
        task4(arr3, 7);

        // task 5

        int[] arr4 = { 1, 2, 3 };
        int[] arr5 = { 4, 5, 6 };
        task5(arr4, arr5);

        // task6
        Object[] arr6 = { 0, 15, false, -22, "html", true, "develop", 47, null };
        task6(arr6);

        // task7
        
        int[] arr7 = { 2, 5, 9, 6 };
        int[] arr8 = { 2, 9, 6 };
        int[] arr9 = { 2, 5, 9, 6 };
        System.out.println("Task 8");
        System.out.println(Arrays.toString(task7(arr7, 5)));
        System.out.println(Arrays.toString(task7(arr8, 5)));
        System.out.println(Arrays.toString(task7(arr9, 6)));

        // task 8
        System.out.println("Task 8");
        int[] arr10={1,2,3,4,5,6,7,8,9};
        int newElement1 = task8(arr10);
        int newElement2 = task8(arr10);
        int newElement3 = task8(arr10);

        System.out.println("Random lần 1: "+newElement1);
        System.out.println("Random lần 2: "+newElement2);
        System.out.println("Random lần 3: "+newElement3);
        //
         //task 9: tạo 1 mảng gồm x phần tử có giá trị y
         System.out.println("Task 9");
         System.out.println(Arrays.toString(task9(6, 0)));
         System.out.println(Arrays.toString(task9(4, 11)));

         //task 10: tạo 1 mảng gồm x phần tử có giá trị y
         System.out.println("Task 10");
         int[] arrX_Y = task10(4, 1);
         System.out.println(Arrays.toString(arrX_Y));
         System.out.println(Arrays.toString(task10(4, -6)));
 
 
    }

    // method task 1: check is arr
    public static boolean task1(Object obj) {
        return obj instanceof Object[] || obj instanceof int[] || obj instanceof long[] || obj instanceof double[]
                || obj instanceof float[] || obj instanceof boolean[] || obj instanceof byte[]
                || obj instanceof short[];
    }

    // method task 2: Trả về phần tử thứ n của mảng
    public static Object task2(int[] arrayOfInts, int index) {
        if (index < arrayOfInts.length) {
            return arrayOfInts[index];
        } else {
            return null;
        }
    }

    // method task 3: Sắp xếp giá trị các phần tử của mảng theo thứ tự tăng dần
    public static void task3(int[] arr) {
        int temp = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] > arr[j]) {
                    temp = arr[j];
                    arr[j] = arr[i];
                    arr[i] = temp;
                }
            }
        }
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + "\t");
        }
    }

    // task 4 : tìm vị trí index của phần tử n trong mảng
    public static void task4(int[] arr, int n) {
        if (n < arr.length) {
            for (int i = 0; i < arr.length; i++) {
                if (arr[i] == n) {
                    System.out.println("Subtask 4: Vị trí của phần tử " + n + " trong mảng là " + i);
                    return;
                }
            }
        } else {
            System.out.println("Subtask 4: Phần tử có giá trị " + n + " không tìm được vị trí trong mảng");

        }
    }

    // task 5 : nối 2 mảng thành một mảng mới
    public static void task5(int arr1[], int arr2[]) {
        int[] result = new int[arr1.length + arr2.length];
        int index = 0;
        for (int i = 0; i < arr1.length; i++) {
            result[index++] = arr1[i]; // đưa mảng 1 vào
        }
        for (int i = 0; i < arr2.length; i++) {
            result[index++] = arr2[i]; // đưa mảng 1 vào
        }
        System.out.println("Mảng ghép được là: " + Arrays.toString(result));

    }

    // task 6 : Lọc một mảng trả ra một mảng chứa các phần tử có giá trị (số hoặc
    // chuỗi)
    public static void task6(Object[] arr) {
        List<Object> list = new ArrayList<>();
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] instanceof Number || arr[i] instanceof String) {
                list.add(arr[i]);
            }
        }
        System.out.println("task6: Mảng sau khi lọc là: " + list);

    }

    // task 7: Bỏ phần tử có giá trị bằng n trong mảng cho trước
    public static int[] task7(int[] arr, int n) {
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == n) {
                count++;
            }
        }
        int[] newArr = new int[arr.length - count];
        int j = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != n) {
                newArr[j] = arr[i];
                j++;
            }
        }
        return newArr;
    }
     // task 8:Lấy random một phần tử bất kỳ trong mảng
    public static int task8(int[]arr){
        Random rd = new Random();
        int index = rd.nextInt(arr.length);
        return arr[index];


    }
    // task9 : Tạo một mảng gồm x phần tử có giá trị y

    public static int[] task9(int x, int y) {
        int[] arr = new int[x];
        for (int i = 0; i < x; i++) {
            arr[i] = y;
        }
        return arr;
    }
    // task 10 :Tạo một mảng gồm y số liên tiếp bắt đầu từ giá trị x
    public static int[] task10(int y, int x) {
        int[] arr = new int[y];
        for (int i = 0; i < y; i++) {
            arr[i] = x ;
            x ++;
        }
        return arr;
    }


}
